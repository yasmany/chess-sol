## Steps to run locally the project

First thing you need to do is install the dependencies.

```sh
npm install
```

After install the dependencies you need to build the client application.

```sh
npm run build
```

Finally you can run the project.

```sh
npm run dev
```

The site should be available in http://localhost:3000
